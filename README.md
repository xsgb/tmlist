# tmlist

Tmlist is a small utility I wrote to list backups from a collection of sparsebundle's Apple timemachine backups.

## Contributions

This still a work in progress project, no release yet. We've planned to write the test suite asap and tag the current set of features as `0.1.0`.
I have many ideas for the evolution of this script, but any suggestions are welcome.

My ideas are:

- Write some tests on the dataset: `test/data/`
- Understand the "bytes" values from the sparsebundle format
- Provide a `size_on_disk` information in the `latest` snapshot
- Add customization for the list output

## Installation

The script actually requires the [tabulate](https://github.com/astanin/python-tabulate) and the [click](https://github.com/pallets/click.git) modules. Ensure they're installed.

```
pip3 install tabulate click
```

```
cd tmlist
python3 setup.py install
```


## Usage

### Help

```
Usage: tmlist [OPTIONS] [MATCH]

  MATCH: pattern given to filter the results (eg: hostname)

  Environment:

  TMLIST_SEARCH_DIR   PATH to be used as the default dir

Options:
  -d, --directory PATH  Base directory for TimeMachine backups. Overrides
                        TMLIST_SEARCH_DIR
  -f, --find            Only list sparsebundle
  -j, --json            Outputs in json
  -i, --indent INTEGER  Indentation value for json formatter
  -l, --list            Outputs in list
  --help                Show this message and exit.
```

### Examples

Specify a directory to search sparsebundle with `-d DIR`, defaults to `.`:
```
$ tmlist -d /backups/timemachine
TimeMachine         Latest backup
------------------  -------------------
tm-mirabelle-90440  2019-11-25 10:16:16
tm-litchi-91061     2019-11-25 03:23:02
tm-dhcp-12-90340    2019-11-25 10:05:40
tm-tomate-91107     2019-11-25 10:03:44
tm-raisin-91060     2019-11-23 00:36:41
tm-citron-90349     2019-11-25 10:09:03
tm-noix-91055       2019-10-28 18:23:45
tm-mangue-91059     2019-11-25 10:52:33
tm-orange-91106     2019-11-25 10:47:03
tm-framboise-90399  2019-11-25 10:32:19
tm-cerise-90341     2019-11-25 09:56:55
```

Limit the list with the `match` argument:
```
$ tmlist framboise
TimeMachine         Latest backup
------------------  -------------------
tm-framboise-90399  2019-11-25 10:32:19
```

Outputs in JSON with `-j` instead of the default human-readable list:
```
$ tmlist -j framboise
{
    "tm-framboise-90399": {
        "2019-10-14-115723": {
            "bytes": 11524915678,
            "completion": "2019-10-14T09:57:23"
        },
        "2019-10-21-030456": {
            "bytes": 103850287,
            "completion": "2019-10-21T01:04:56"
        },
        "2019-11-21-143229": {
            "bytes": 6273593925,
            "completion": "2019-11-21T13:32:29"
        },
        "2019-11-22-134105": {
            "bytes": 790869971,
            "completion": "2019-11-22T12:41:05"
        },
        "2019-11-25-034448": {
            "bytes": 66284795,
            "completion": "2019-11-25T02:44:48"
        },
        "2019-11-25-051410": {
            "bytes": 6408867,
            "completion": "2019-11-25T04:14:10"
        },
        "2019-11-25-090156": {
            "bytes": 12120232,
            "completion": "2019-11-25T08:01:56"
        },
        "2019-11-25-100250": {
            "bytes": 48971352,
            "completion": "2019-11-25T09:02:50"
        },
        "2019-11-25-113219": {
            "bytes": 11608511,
            "completion": "2019-11-25T10:32:19"
        },
        "latest": {
            "bytes": 11608511,
            "completion": "2019-11-25T10:32:19",
            "name": "2019-11-25-113219",
            "date": "2019-11-25T10:32:19",
            "bandsize": 268435456,
            "sparsebundle": "/home/xasa/doc/fjs-sxb-iac/utils/fjs-tmlist/tmlist/tests/data/tm-framboise-90399/framboise.backupbundle"
        }
    }
}
```
