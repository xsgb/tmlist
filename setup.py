"""
setup for tmlist
"""

from setuptools import setup, find_packages


setup(
    name="tmlist",
    version="0.1.0",
    description="Lists backups from a collection of sparsebundle's Apple timemachine backups",
    author="XS",
    author_email="xs@inda.re",
    url="",
    packages=find_packages(exclude=("tests", "docs")),
    entry_points="""
        [console_scripts]
        tmlist=tmlist.tmlist:cli
    """,
    tests_require=["pytest"],
    install_requires=["tabulate", "click", ],
    data_files=[("", ["LICENSE"])],
)
