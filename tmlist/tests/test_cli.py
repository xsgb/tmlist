"""
Tests for the CLI interface
"""

import pathlib
from subprocess import os
import json
from click.testing import CliRunner
import tmlist

CWD = os.getcwd()
DATA_DIR = f"{CWD}/tmlist/tests/data"
NO_DATA_DIR = f"{CWD}/tmlist/tests/no_bundles"
ENV = {}


def run_tmlist(
    args: list = None, testdir: pathlib.Path = CWD, testenv=None
) -> object:
    """
    CliRunner wrapper to test tmlist's CLI
    """
    os.chdir(testdir)
    runner = CliRunner(mix_stderr=False, env=testenv)
    result = runner.invoke(tmlist.cli, args=args, env=testenv)
    os.chdir(CWD)
    return result


def test_tmlist():
    """
    Test tmlist with no args in a dir without sparsebundles
    Expected: fail
    """
    result = run_tmlist(testdir=NO_DATA_DIR)
    assert result.exit_code == 2
    assert result.stderr.startswith("No sparsebundle found in:")
    assert CWD in result.stderr


def test_tmlist_with_sparsebundles():
    """
    Test tmlist with no args in a dir with sparsebundles
    """
    result = run_tmlist(testdir=DATA_DIR)
    assert result.exit_code == 0
    assert result.output.startswith("TimeMachine")
    assert "Latest backup" in result.output


def test_tmlist_directory_option():
    """
    Test tmlist -d PATH option
    """
    result = run_tmlist(testdir=NO_DATA_DIR, args=["-d", DATA_DIR])
    assert result.exit_code == 0
    assert result.output.startswith("TimeMachine")
    assert "Latest backup" in result.output


def test_tmlist_find_option():
    """
    Test tmlist -f option
    """
    result = run_tmlist(testdir=NO_DATA_DIR, args=["-f"])
    assert result.exit_code == 2
    assert result.stderr.startswith("No sparsebundle found in:")
    assert CWD in result.stderr


def test_tmlist_find_option_sparsebundles():
    """
    Test tmlist -f option with sparsebundle in CWD
    """
    result = run_tmlist(args=["-f"], testdir=DATA_DIR)
    assert result.exit_code == 0
    assert result.output.startswith("/")
    assert "bundle" in result.output


def test_tmlist_json_option_sparsebundles():
    """
    Test tmlist -j options with sparsebundles in CWD
    """
    result = run_tmlist(args=["-j"], testdir=DATA_DIR)
    assert json.loads(result.output)


def test_tmlist_json_option_sparsebundles_from_env():
    """
    Test tmlist -j options with sparsebundles from env
    """
    result = run_tmlist(args=["-j"], testenv={"TMLIST_DIRECTORY": DATA_DIR})
    assert json.loads(result.output)
    print(result.output)
