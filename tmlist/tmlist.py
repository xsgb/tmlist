#!/usr/bin/env python3

"""
List timemachine backups from sparsebundles backup directory
"""

from xml.parsers.expat import ExpatError
import datetime
import glob
import json
import os
import sys
import pathlib
import plistlib
import click
import tabulate

BUNDLES_GLOB = "*.*bundle"
PLIST_HISTORY = "com.apple.TimeMachine.SnapshotHistory.plist"
PLIST_INFO = "Info.plist"
# TMID_PART = 0  # TODO: rework the search globbing to be more flexible

# Ensure datetime is correctly encoded for json output
json.JSONEncoder.default = lambda self, obj: (
    obj.isoformat() if isinstance(obj, datetime.datetime) else None
)


def find_backups(
    path: pathlib.Path, limit: str = None, glob_pattern: str = BUNDLES_GLOB
) -> list:
    """
    Finds matching sparsebundles from the given path using glob.
    """
    path = pathlib.Path(os.path.abspath(path))
    bundles = sorted(path.rglob(f"*{limit}*/{glob_pattern}"))
    try:
        bundles[0]
    except IndexError:
        click.echo(f'No sparsebundle found in: "{path}"', err=True)
        sys.exit(2)
    return bundles


def load_plist(filename: pathlib.Path) -> dict:
    """
    Loads a plist file into a dict.
    """
    try:
        with open(filename, mode="rb") as fname:
            data = plistlib.load(fname)
    except (PermissionError, ExpatError, FileNotFoundError) as error:
        click.echo(f"{error}, {data}", err=True)
    return data if data else None


# pylint: disable-msg=too-many-locals
def load_backups(bundles: list) -> dict:
    """
    List snapshots and creates a latest backup entry for each given
    sparsebundle by loading the .plist files.
    """
    backups = {}
    for dest in bundles:
        dirname = pathlib.Path(dest)
        tmid = dirname.parts[-2]
        snapshots = {}
        names = []
        dates = []
        backups[tmid] = {}

        # load Snapshots plist
        snapshots_data = load_plist(pathlib.Path(dest, PLIST_HISTORY))

        # load Infos plist
        info_data = load_plist(pathlib.Path(dest, PLIST_INFO))

        # Get all snapshots
        for snapshot in snapshots_data["Snapshots"]:
            name = snapshot["com.apple.backupd.SnapshotName"]
            completion = snapshot["com.apple.backupd.SnapshotCompletionDate"]
            size = snapshot["com.apple.backupd.SnapshotTotalBytesCopied"]
            snapshots[f"{name}"] = {"bytes": size, "completion": completion}

        # Determine the latest backup from snapshot's dates
        for key, value in snapshots.items():
            names.append(key)
            dates.append(value.get("completion").isoformat())

        names.sort()
        dates.sort()

        snapshots["latest"] = {
            **snapshots[names[-1]],
            "name": names[-1],
            "date": dates[-1],
            "bandsize": info_data["band-size"],
            "sparsebundle": f"{dest}",
        }

        backups[tmid] = {**snapshots}

    return backups


def dump_json(backups, indent):
    """
    Dump backups informations to json.
    """
    print(json.dumps(backups, indent=indent))


def dump_list(backups):
    """
    Pretty print backups informations to a list
    """
    table = {}
    table_headers = ["TimeMachine", "Latest backup"]

    for tmid, snapshots in backups.items():
        table[tmid] = str(snapshots["latest"]["completion"])

    print(tabulate.tabulate(table.items(), headers=table_headers))


# Command Line Utility
@click.command()
@click.argument("match", default="", type=str, required=False)
@click.option(
    "-d",
    "--directory",
    help="Base directory for TimeMachine backups. Overrides TMLIST_DIRECTORY",
    type=pathlib.Path,
    default=".",
    envvar="TMLIST_DIRECTORY",
)
@click.option(
    "-f", "--find", "output", flag_value="find", help="Only list sparsebundle"
)
@click.option(
    "-j", "--json", "output", flag_value="json", help="Outputs in json"
)
@click.option(
    "-i", "--indent", default=4, help="Indentation value for json formatter"
)
@click.option(
    "-l",
    "--list",
    "output",
    flag_value="list",
    help="Outputs in list",
    default="list",
)
def cli(directory, output, indent, match):
    """
    MATCH: pattern given to filter the results (eg: hostname)

    Environment:

    TMLIST_DIRECTORY   PATH to be used as the default dir
    """
    data = load_backups(find_backups(path=directory, limit=match))
    if output == "find":
        for sparsebundle in find_backups(path=directory, limit=match):
            print(sparsebundle)
    if output == "json":
        dump_json(data, indent)
    if output == "list":
        dump_list(data)


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    cli()
